# Grunt more.
!SLIDE
# Grunt More

![images/grunt-logo.png](images/grunt-logo.png)

!SLIDE
## What is grunt?

![images/grunt-question.png](images/grunt-question.png)

!SLIDE
## Javascript Task Runner.
Built on `Node.js`
![images/grunt-logo.png](images/grunt-logo.png)

!SLIDE
## What the f*** is Node.js?
![images/node-logo.png](images/node-logo.png)


!SLIDE
### The official description
>"Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, perfect for data-intensive real-time applications that run across distributed devices."

!SLIDE
### In a nutshell
- Non-blocking server-side javascript.
- Apps can be totally written in javascript, including database components.
- Hal is written in node.js

!SLIDE
### Who uses Node.js?

- Lots of Hipsters
- Ghost (http://tryghost.org)
- Github (http://hubot.github.com)
- Lots of big players, you can find a list at `http://nodejs.org/industry/`

!SLIDE
## Okay, but what the f*** is a task runner?

Er, It runs tasks for you...

*Duh...*

!SLIDE
## Smartarse. What tasks?
Things you would **normally do manually** using several different applications can now be defined in your `Gruntfile.js` at your project's root, grunt will now perform all of these for you.

These tasks are then ran using the `grunt` command.

```bash
grunt css:minify
```


!SLIDE solarized
![images/node-logo.png](images/less-logo.png)
Like... Parse less and *concatenate all the files together* in one go.

```javascript
less: {
    development: {
        options: {
            compress: false
        }
        files: {
            "./public/assets/css/style.css":"./public/assets/less/style.less"
            "./public/assets/css/survey.css": "./public/assets/less/survey.less"
        }
    }
}
```

!SLIDE
![images/node-logo.png](images/sass-logo.png)
... Or SASS if you're that way inclined.

```javascript
sass: {
    development: {
        options: {
            compress: false
        }
        files: {
            "./public/assets/css/style.css":"./public/assets/sass/style.scss"
            "./public/assets/css/survey.css": "./public/assets/sass/survey.scss"
        }
    }
}
```

!SLIDE
![JSHint](images/jshint-dark.png)

Like... use JSHint to check how good your Javascript is.

```bash
grunt js:check
```

!SLIDE
## Minify and optimise images

Grunt can optimise image sizes, useful for sprites and theme images to make them even faster!

`https://github.com/gruntjs/grunt-contrib-imagemin`

!SLIDE
## Watch files

Watch your files for changes and automatically run tasks against them.

```bash
grunt watch
```

```javascript
watch: {
    less: {
        files: ['./public/assets/less/*.*'],
        tasks: ['less'],
    },
    coffee: {
        files: ['./public/assets/coffee/*.coffee'],
        tasks: ['coffee', 'concat:javascript'],
    },
}
```

!SLIDE
![Steps](images/apps.png)
## It replaces many steps with one workflow.

- Grunt uses a `.Gruntfile`, that would allow all developers to work with the same settings and standards.
- Standardisation is the name of the game, every developer gets the same tool with the same results.

!SLIDE
<img src="images/any_of_those_things.gif" width="800" />

I know what you're thinking...

!SLIDE
## Getting started

!SLIDE

### Get your project

Checkout the repo of the site you're working on and go to the project root.

```shell
git checkout myproject
cd myproject
```
!SLIDE

### Make sure you have NodeJS Installed:

Installation is easy:
- Get the package for your OS from http://nodejs.org/download/
- Install it!

On a mac it's even easier:

```bash
# Install homebrew
ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

# Install node using homebrew
brew install node
```

!SLIDE
### Install grunt and it's dependancies.

First time install? Run:

```bash
npm install grunt-cli --save-dev
```

Already have a package.json file? Just run:

```bash
npm install
```

!SLIDE
## Gruntfile.js / Gruntfile.coffee

This file defines all your tasks as simple Javascript objects, take a look at Simply Satisfied's LESS task:

```javascript

// Wrapper function
module.exports = function(grunt) {

    // This is the function that sets the config
    grunt.initConfig({

        // This is a task.
        less: {

            // This is a subtask
            development: {

                options: {
                    compress: true
                }
                // Define files like "destination" : ""
                files: {
                    "./public/assets/css/style.css":"./public/assets/less/style.less"
                }
            }
        }
    });
}
```

!SLIDE
### Grunt plugins

All the functionality for tasks is provided by plugins found at: http://gruntjs.com/plugins

![Grunt Plugins](images/plugins.png)

Those that start with `contrib-` are semi-official and the most supported.

!SLIDE
## Package.json

Lives at the root of your project, and lists the plugins you're using.

This is how NodeJS knows what plugins to install, including grunt, when you run `npm install`.

```json
{
    "name": "simply-laravel",
    "main": "Gruntfile.js",
    "author": "Dan Matthews",
    "devDependencies": {
        "grunt": "~0.4.2",
        "grunt-contrib-concat": "~0.3.0",
        "grunt-contrib-less": "~0.8.3",
        "grunt-contrib-uglify": "~0.2.7",
        "grunt-contrib-watch": "~0.5.3",
        "grunt-contrib-coffee": "~0.7.0"
    }
}
```

!SLIDE

![Drupalicon](images/drupalicon.png)
## Do we need all that for drupal development?

Some of it we don't, but some of it may make our lives easier in the long run.


!SLIDE
## What could we use it for?

!SLIDE
### JSHint
JSHint may improve code quality of Javascript we write to be consistent between all of us.

!SLIDE
### PHP Code Sniffer

PHP code sniffer might improve the quality of the PHP we write, keeping a consistent Drupal code style in PHP files.

`https://github.com/SaschaGalley/grunt-phpcs`

!SLIDE
### CSS & LESS

- We could locally parse, minimise and concatenate common CSS & JS libraries into smaller files (Starter theme dan?)

!SLIDE
### Automatically clear Drupal's cache

Grunt on the development server could watch for changes in certain folders, then automatically clear the cache for us using Drush.

!SLIDE
### Preparing for production

We could use a `grunt:production` task to prepare javascript, CSS files and images for production and deployment, adding header comments that name the author of the file (Hydrant), strip `console.log` statements etc.

!SLIDE
## Plugins are pretty easy to write.

If you've got any ideas about things we could automate for Drupal sites, shout up!

!SLIDE

## Automate everything that sucks.

That's a direct quote from Github founder *Zach Holman*.

We're getting better at being more efficient at the shitty repetitive stuff.

!SLIDE
<img src="images/uhoh.gif" width="400" />
## Questions?

This is the end, so it's probably the best time to ask them.

Slides are available at: `https://bitbucket.org/danmatthews/grunt-more`
